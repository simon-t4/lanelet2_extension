#include <lanelet2_extension/projection/mgrs_projector.hpp>
// #include <lanelet2_extension/autoware_traffic_light.hpp>
#include <lanelet2_core/primitives/BasicRegulatoryElements.h>

#include <lanelet2_core/primitives/Lanelet.h>
#include <lanelet2_io/Io.h>
#include <lanelet2_io/io_handlers/Factory.h>
#include <lanelet2_io/io_handlers/Writer.h>
#include <lanelet2_projection/UTM.h>

#include <lanelet2_extension/regulatory_elements/autoware_traffic_light.hpp>
#include <iostream>

int main(int argc, char *argv[])
{
    using namespace lanelet;

    const std::string map_file_path = "/home/simon/work/peoria_data/map/Autoware_lanelet2/Peoria_autoware_lanelet_20190702.osm";

    lanelet::LaneletMapPtr lanelet_map;
    ErrorMessages errors;
    lanelet::projection::MGRSProjector projector;
    lanelet_map = load(map_file_path,"autoware_osm_handler", projector, &errors);

    for ( auto lanelet: lanelet_map->laneletLayer )
    {
        //You can access to traffic light element as AutowareTrafficLight class
        auto autoware_traffic_lights = lanelet.regulatoryElementsAs<lanelet::autoware::AutowareTrafficLight>();
        if(autoware_traffic_lights.empty()) continue;
        for (auto light: autoware_traffic_lights)
        {
            std::cout << "light_bulbs: ";
            for(auto light_string: light->lightBulbs() )
            {
                std::cout << light_string.id() << " ";
            }
            std::cout << std::endl;
        }

        //You can also access to traffic light element as default TrafficLight class
        auto traffic_lights = lanelet.regulatoryElementsAs<lanelet::TrafficLight>();
        for (auto light: traffic_lights)
        {
            std::cout << "traffic lights: ";
            for(auto light_string: light->trafficLights() )
            {
                std::cout << light_string.id() << " ";
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }


    return 0;
}
