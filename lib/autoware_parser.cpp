#include <lanelet2_extension/io/autoware_parser.hpp>
#include <lanelet2_io/io_handlers/OsmHandler.h>
#include <lanelet2_io/io_handlers/OsmFile.h>
#include <lanelet2_io/io_handlers/Factory.h>
#include <string>
namespace lanelet {
namespace io_handlers {

std::unique_ptr<LaneletMap> AutowareOsmParser::parse(const std::string& filename, ErrorMessages& errors) const
{
  using namespace std::literals::string_literals;
  auto map = OsmParser::parse(filename, errors);

  // pugi::xml_document doc;
  // auto result = doc.load_file(filename.c_str());
  // if (!result) 
  // {
  //   throw lanelet::ParseError("Errors occured while parsing osm file: "s + result.description());
  // }
  // 
  // auto metainfo = doc.child("MetaInfo"); 
  // if( metainfo.attribute("format_version"))
  //   std::string format_version = metainfo.attribute("format_version").value();
  // if( metainfo.attribute("map_version"))
  //   std::string map_version = metainfo.attribute("map_version").value();

  for ( Point3d point: map->pointLayer)
  {
    if(point.hasAttribute("local_x"))
    {
      point.x() = point.attribute("local_x").asDouble().value();
    }
    if(point.hasAttribute("local_y"))
    {
      point.y() = point.attribute("local_y").asDouble().value();
    }
  }
  return map;
}

namespace{
  RegisterParser<AutowareOsmParser> regParser;
}

}
}
