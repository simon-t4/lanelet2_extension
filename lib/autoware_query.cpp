#include "ros/ros.h"
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/CameraInfo.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PolygonStamped.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <jsk_recognition_msgs/PolygonArray.h>
#include "std_msgs/String.h"





#include <lanelet2_core/primitives/Lanelet.h>
#include <Eigen/Eigen>


#include <lanelet2_extension/query/autoware_query.h>

#include <cstdio>

#include <sstream>
//#include "libLaneletMap.h"



#include <unistd.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <string>

using namespace lanelet_utils;

double hypot(geometry_msgs::Point32 p0, geometry_msgs::Point32 p1)
{
  return (sqrt(pow((p1.x -p0.x),2.0) + pow((p1.y - p0.y),2.0)));
}


double adjacentPoints(int i, int N, geometry_msgs::Polygon poly, geometry_msgs::Point32 & p0,  geometry_msgs::Point32 & p1,  geometry_msgs::Point32 & p2)
{
  
  p1 = poly.points[i];
  if (i ==0) 
    p0 = poly.points[N-1];
  else p0 = poly.points[i-1];
  
  if (i < N-1) 
    p2 = poly.points[i+1];
  else
    p2 = poly.points[0];
}

void Query::lanelet2Triangle(lanelet::Lanelet ll, std::vector<geometry_msgs::Polygon> & triangles)
{

  triangles.clear();
  geometry_msgs::Polygon ll_poly;
  lanelet2Polygon(ll, ll_poly);

  //ear clipping: find smallest internal angle in polygon
  int N = ll_poly.points.size();



  while (N >= 3) {

    double min_angle = 2*M_PI;
    int min_i = -1;

    for (int i =0; i < N; i++) {
      geometry_msgs::Point32 p0,p1,p2;
      
      adjacentPoints(i, N, ll_poly, p0, p1, p2);
      // angle at vertex i formed by p0, p1, p2
      
      double a = hypot(p0,p1);
      double b = hypot(p1,p2);
      double c = hypot(p0,p2);
      double theta = acos((a*a + b*b - c*c)/(2.0*a*b));
      if (theta < min_angle) {
	min_angle = theta;
	min_i = i;
      }
    }
    if (min_i >=0 && min_i < N) {
      geometry_msgs::Point32 p0,p1,p2;
      adjacentPoints(min_i, N, ll_poly, p0, p1, p2);
      geometry_msgs::Polygon triangle;
      triangle.points.push_back(p0);
      triangle.points.push_back(p1);
      triangle.points.push_back(p2);
      triangles.push_back(triangle);
      
      // remove vertex of center of angle
      auto it = ll_poly.points.begin();
      std::advance(it, min_i);
      ll_poly.points.erase(it);
      
    }
    N = ll_poly.points.size();
  }
  
}

void Query::lanelet2Polygon(lanelet::Lanelet ll, geometry_msgs::Polygon& polygon)
{

  lanelet::ConstLineString3d left_ls = ll.leftBound();
  lanelet::ConstLineString3d right_ls = ll.rightBound();

  int polygon_size= left_ls.size() +right_ls.size();
  
  for (auto i = left_ls.begin(); i != left_ls.end(); i++) {
    geometry_msgs::Point32 p;
    
    p.x = (*i).x();
    p.y = (*i).y();
    p.z = (*i).z();
    polygon.points.push_back(p);
    
  }
  
  for (auto i = right_ls.end(); i != right_ls.begin();) {
    i--;
    geometry_msgs::Point32 p;
 
   p.x = (*i).x();
   p.y = (*i).y();
   p.z = (*i).z(); 
   polygon.points.push_back(p);
    
  }
  
  
}



// returns all lanelets in laneletLayer - don't know how to convert PrimitveLayer<Lanelets> -> std::vector<Lanelets>
lanelet::Lanelets Query::laneletLayer(lanelet::LaneletMapPtr ll_map)
{
  lanelet::Lanelets lanelets;

  for (auto li = ll_map->laneletLayer.begin(); li !=ll_map->laneletLayer.end(); li++) {
   
    lanelets.push_back(*li);
  }

  return lanelets;
}

lanelet::Lanelets Query::subtypeLanelets(lanelet::Lanelets lls, const char subtype[])
{
  lanelet::Lanelets subtype_lanelets;

  
  for (auto li = lls.begin(); li !=lls.end(); li++) {
    lanelet::Lanelet ll = *li;
    
    if (ll.hasAttribute(lanelet::AttributeName::Subtype)){
      lanelet::Attribute attr = ll.attribute(lanelet::AttributeName::Subtype);
      if (attr.value() == subtype) {
	subtype_lanelets.push_back(ll);
      }
    }
     
  }
  
  //std::cerr << "Found " << cross_walks.size() << " crosswalk lanelets\n";
  return subtype_lanelets;
}
lanelet::Lanelets Query::crosswalkLanelets(lanelet::Lanelets lls)
{

  return (Query::subtypeLanelets(lls, lanelet::AttributeValueString::Crosswalk));
}

lanelet::Lanelets Query::roadLanelets(lanelet::Lanelets lls)
{
  return (Query::subtypeLanelets(lls, lanelet::AttributeValueString::Road));
}

std::vector<lanelet::TrafficLight::Ptr> Query::trafficLights(lanelet::Lanelets lanelets)
{
  std::vector<lanelet::TrafficLight::Ptr> tl_reg_elems;

  for (auto i = lanelets.begin(); i != lanelets.end(); i++) {
    
    lanelet::Lanelet ll = *i;
    std::vector<lanelet::TrafficLight::Ptr> ll_tl_re = ll.regulatoryElementsAs<lanelet::TrafficLight>();

    // insert unique tl into array
    for (auto tli = ll_tl_re.begin(); tli != ll_tl_re.end(); tli++) {

      lanelet::TrafficLight::Ptr tl_ptr = *tli;
      lanelet::Id id = tl_ptr->id();
      bool unique_id = true;
      for  (auto ii = tl_reg_elems.begin(); ii != tl_reg_elems.end(); ii++){
	if (id == (*ii)->id()){
	  unique_id = false;
	  break;
	}
      }
      if (unique_id) tl_reg_elems.push_back(tl_ptr);
	
    }
  }
  return tl_reg_elems;
}



std::vector<lanelet::autoware::AutowareTrafficLight::Ptr> Query::autowareTrafficLights(lanelet::Lanelets lanelets)
{
  std::vector<lanelet::autoware::AutowareTrafficLight::Ptr> tl_reg_elems;

  for (auto i = lanelets.begin(); i != lanelets.end(); i++) {
    
    lanelet::Lanelet ll = *i;
    std::vector<lanelet::autoware::AutowareTrafficLight::Ptr> ll_tl_re = ll.regulatoryElementsAs<lanelet::autoware::AutowareTrafficLight>();

    // insert unique tl into array
    for (auto tli = ll_tl_re.begin(); tli != ll_tl_re.end(); tli++) {

      lanelet::autoware::AutowareTrafficLight::Ptr tl_ptr = *tli;
      lanelet::Id id = tl_ptr->id();
      bool unique_id = true;
      for  (auto ii = tl_reg_elems.begin(); ii != tl_reg_elems.end(); ii++){
	if (id == (*ii)->id()){
	  unique_id = false;
	  break;
	}
      }
      if (unique_id) tl_reg_elems.push_back(tl_ptr);
	
    }
  }
  return tl_reg_elems;
}


// return all stop lines and ref lines from a given set of lanelets
std::vector<lanelet::ConstLineString3d> Query::stopLinesLanelets(lanelet::Lanelets lanelets)
{
  std::vector<lanelet::ConstLineString3d> stoplines;

  for (auto lli = lanelets.begin(); lli != lanelets.end(); lli++) {
      std::vector<lanelet::ConstLineString3d> ll_stoplines;
      ll_stoplines = Query::stopLinesLanelet(*lli);
      stoplines.insert(stoplines.end(), ll_stoplines.begin(), ll_stoplines.end());
  }
  return stoplines;
}

// return all stop and ref lines from a given lanel
std::vector<lanelet::ConstLineString3d> Query::stopLinesLanelet(lanelet::Lanelet ll)
{ 
  std::vector<lanelet::ConstLineString3d> stoplines;

  // find stop lines referened by right ofway reg. elems.
  std::vector<std::shared_ptr<const lanelet::RightOfWay>> right_of_way_reg_elems = ll.regulatoryElementsAs<const lanelet::RightOfWay>();
  
  if (right_of_way_reg_elems.size() > 0)
    {
    
      // lanelet has a right of way elem elemetn
      for (auto j = right_of_way_reg_elems.begin(); j < right_of_way_reg_elems.end(); j++)
	{
	  if ((*j)->getManeuver(ll) == lanelet::ManeuverType::Yield)
	    {
	      // lanelet has a yield reg. elem.
	      lanelet::Optional<lanelet::ConstLineString3d> row_stopline_opt = (*j)->stopLine();
	      if (!!row_stopline_opt) stoplines.push_back(row_stopline_opt.get());
	  }
	  
	}
    }
  
  // find stop lines referenced by traffic lights
  std::vector<std::shared_ptr<const lanelet::TrafficLight>> traffic_light_reg_elems = ll.regulatoryElementsAs<const lanelet::TrafficLight>();
  
  if (traffic_light_reg_elems.size() > 0)
    {
	
      // lanelet has a traffic light elem elemetn
      for (auto j = traffic_light_reg_elems.begin(); j < traffic_light_reg_elems.end(); j++)
	{
	  
	  lanelet::Optional<lanelet::ConstLineString3d> traffic_light_stopline_opt = (*j)->stopLine();
	  if (!!traffic_light_stopline_opt) stoplines.push_back(traffic_light_stopline_opt.get());
	  
	}
      
    }
  // find stop lines referenced by traffic signs
  std::vector<std::shared_ptr<const lanelet::TrafficSign>> traffic_sign_reg_elems = ll.regulatoryElementsAs<const lanelet::TrafficSign>();
  
  if (traffic_sign_reg_elems.size() > 0)
    {
      
      // lanelet has a traffic sign reg elem - can have multiple ref lines (but stop sign shod have 1
      for (auto j = traffic_sign_reg_elems.begin(); j < traffic_sign_reg_elems.end(); j++)
	{
	  
	  lanelet::ConstLineStrings3d traffic_sign_stoplines = (*j)->refLines();
	  if (traffic_sign_stoplines.size() > 0) stoplines.push_back(traffic_sign_stoplines.front());
	  
	}
      
    }
  return stoplines;
}

