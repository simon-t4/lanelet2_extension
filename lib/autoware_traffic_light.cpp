#include <boost/variant.hpp>

#include <lanelet2_extension/regulatory_elements/autoware_traffic_light.hpp>
#include <lanelet2_core/primitives/RegulatoryElement.h>

namespace lanelet
{
namespace autoware
{
namespace{
template <typename T>
bool findAndErase(const T& primitive, RuleParameters& member)
{
    auto it = std::find(member.begin(), member.end(), RuleParameter(primitive));
    if (it == member.end()) {
        return false;
    }
    member.erase(it);
    return true;
}

template <typename T>
RuleParameters toRuleParameters(const std::vector<T>& primitives)
{
    return utils::transform(primitives, [] (const auto & elem) { return static_cast<RuleParameter>(elem); });
}

template <>
RuleParameters toRuleParameters(const std::vector<LineStringOrPolygon3d>& primitives)
{
    return utils::transform(primitives, [] (const auto & elem) { return elem.asRuleParameter(); });
}

LineStringsOrPolygons3d getLsOrPoly(const RuleParameterMap& paramsMap, RoleName role)
{
    auto params = paramsMap.find(role);
    if (params == paramsMap.end()) {
        return {};
    }
    LineStringsOrPolygons3d result;
    for (auto& param : params->second)
    {
        auto l = boost::get<LineString3d>(&param);
        if (l != nullptr) {
            result.push_back(*l);
        }
        auto p = boost::get<Polygon3d>(&param);
        if (p != nullptr) {
            result.push_back(*p);
        }
    }
    return result;
}

ConstLineStringsOrPolygons3d getConstLsOrPoly(const RuleParameterMap& params, RoleName role)
{
    return utils::transform(getLsOrPoly(params, role),
                            [] (auto & lsOrPoly) { return static_cast<ConstLineStringOrPolygon3d>(lsOrPoly); });
}

RegulatoryElementDataPtr constructAutowareTrafficLightData(Id id, const AttributeMap& attributes,
                                                           const LineStringsOrPolygons3d& trafficLights,
                                                           const Optional<LineString3d>& stopLine,
                                                           const std::vector<Optional<LineString3d> >& lightBulbs
                                                           )
{
    RuleParameterMap rpm = {{RoleNameString::Refers, toRuleParameters(trafficLights)}};

    if (!!stopLine) {
        rpm.insert({RoleNameString::RefLine, {*stopLine}});
    }
    for (const auto lightBulb : lightBulbs )
    {
        if (!!lightBulb) {
            rpm.insert({AutowareRoleNameString::LightBulbs, {*lightBulb}});
        }
    }

    auto data = std::make_shared<RegulatoryElementData>(id, rpm, attributes);
    data->attributes[AttributeName::Type] = AttributeValueString::RegulatoryElement;
    data->attributes[AttributeName::Subtype] = AttributeValueString::TrafficLight;
    return data;
}
} //namespace

constexpr const char AutowareRoleNameString::LightBulbs[];

AutowareTrafficLight::AutowareTrafficLight(const RegulatoryElementDataPtr& data) : TrafficLight(data)
{
}

AutowareTrafficLight::AutowareTrafficLight(Id id, const AttributeMap& attributes, const LineStringsOrPolygons3d& trafficLights,
                                           const Optional<LineString3d>& stopLine, const std::vector<Optional<LineString3d> >& lightBulbs )
    : TrafficLight(id, attributes, trafficLights, stopLine)
{
   for (const auto &lightBulb : lightBulbs )
   {
       if (!!lightBulb) {
         addLightBulbs(*lightBulb);
       }
   }
}

ConstLineStrings3d AutowareTrafficLight::lightBulbs() const
{
  return getParameters<ConstLineString3d>(AutowareRoleNameString::LightBulbs);
}

void AutowareTrafficLight::addLightBulbs(const LineStringOrPolygon3d& primitive)
{
    parameters()[AutowareRoleNameString::LightBulbs].emplace_back(primitive.asRuleParameter());
}

bool AutowareTrafficLight::removeLightBulbs(const LineStringOrPolygon3d& primitive)
{
    return findAndErase(primitive.asRuleParameter(), parameters().find(AutowareRoleNameString::LightBulbs)->second);
}

  #if __cplusplus < 201703L
constexpr char AutowareTrafficLight::RuleName[];      // instanciate string in cpp file
#endif

}
}
