#include "ros/ros.h"
#include <tf/tf.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/CameraInfo.h>
#include <geometry_msgs/TwistStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PolygonStamped.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

#include <jsk_recognition_msgs/PolygonArray.h>
#include "std_msgs/String.h"





#include <lanelet2_core/primitives/Lanelet.h>
#include <Eigen/Eigen>

#include <cstdio>

#include <sstream>
//#include "libLaneletMap.h"

#include <unistd.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <string>


#include <lanelet2_extension/query/autoware_query.h>
#include <lanelet2_extension/visualization/autoware_visualization.h>


using namespace lanelet_utils;


bool isAttributeValue(lanelet::ConstPoint3d p, std::string attr_str, std::string value_str)
{
  lanelet::Attribute attr = p.attribute(attr_str);
  if (attr.value().compare(value_str) == 0) 
    return true;
  return false;
}
bool isLaneletAttributeValue(lanelet::Lanelet ll, std::string attr_str, std::string value_str)
{
  lanelet::Attribute attr = ll.attribute(attr_str);
  if (attr.value().compare(value_str) == 0) 
    return true;
  return false;
}


void setGeomMsgPt32(geometry_msgs::Point32 & p, Eigen::Vector3d & v)
{
  p.x = v.x(); p.y =v.y(); p.z = v.z();
}
void setGeomMsgPt(geometry_msgs::Point & p, Eigen::Vector3d & v)
{
  p.x = v.x(); p.y =v.y(); p.z = v.z();
}
 void setGeomMsgPtFromPt32(geometry_msgs::Point & p, geometry_msgs::Point32 & p32)
{
  p.x = p32.x; p.y = p32.y; p.z = p32.z;
}


void lightAsMarker(lanelet::ConstPoint3d p, visualization_msgs::Marker & marker, int id, std::string ns)
{	
  marker.header.frame_id = "map";
  marker.header.stamp = ros::Time();
  marker.ns = ns;
  marker.id = id;
  marker.type = visualization_msgs::Marker::SPHERE;
  marker.pose.position.x = p.x();
  marker.pose.position.y = p.y();
  marker.pose.position.z = p.z();
  
  float s = 0.3;
	
  marker.scale.x = s;
  marker.scale.y = s;
  marker.scale.z = s;
  
  marker.color.r = 0.0f;
  marker.color.g = 0.0f;
  marker.color.b = 0.0f;
  marker.color.a = 1.0f;

  if (isAttributeValue(p, "color", "red"))
    marker.color.r = 1.0f;
  else if (isAttributeValue(p, "color", "green"))
    marker.color.g = 1.0f;
  else if (isAttributeValue(p, "color", "yellow")){
    marker.color.r = 1.0f;
    marker.color.g = 1.0f;
  }
  else {
    
    marker.color.r = 1.0f;
    marker.color.g = 1.0f;
    marker.color.b = 1.0f;
      
  }
  marker.lifetime = ros::Duration();


}

void laneletDirectionAsMarker(lanelet::Lanelet ll, visualization_msgs::Marker & marker, int id, std::string ns)
{

  marker.header.frame_id = "map";
  marker.header.stamp = ros::Time();
  marker.ns = ns;
  marker.id = id;
  marker.type = visualization_msgs::Marker::TRIANGLE_LIST;

      lanelet::BasicPoint3d pt[3];
    pt[0].x() = 0.0;
    pt[0].y() = -0.3;
    pt[0].z() = 0.0;
    pt[1].x() = 0.0;
    pt[1].y() = 0.3;
    pt[1].z() = 0;
    pt[2].x() = 1.0;
    pt[2].y() = 0.0;
    pt[2].z() = 0;

  lanelet::BasicPoint3d p, epl, epr, pc, pc2;
  // epl = ll.leftBound().back();
  // epr = ll.rightBound().back();
  //epc = ll.centerline().back();

  lanelet::ConstLineString3d center_ls = ll.centerline();
  float s = 1.0;
    

  
  marker.lifetime = ros::Duration();

  p = center_ls.front();
 
  marker.pose.position.x = 0.0;//p.x();
  marker.pose.position.y = 0.0;//p.y();
  marker.pose.position.z = 0.0;//p.z();
      marker.scale.x = s;
  marker.scale.y = s;
  marker.scale.z = s;
  marker.color.r = 1.0f;
  marker.color.g = 1.0f;
  marker.color.b = 1.0f;
  marker.color.a = 1.0f;

  
  lanelet::Attribute attr = ll.attribute("turn_direction");
  double turn_dir = 0;

  std_msgs::ColorRGBA c;
  c.r = 0.0; c.g = 0.0; c.b = 1.0; c.a = 0.6;
  
  if (isLaneletAttributeValue(ll, "turn_direction", "right")) {
    turn_dir = -M_PI/2.0;
    c.r = 1.0; c.g = 0.0; c.b = 1.0;

  }
  else if (isLaneletAttributeValue(ll, "turn_direction", "left")) {
    turn_dir = M_PI/2.0;
    c.r = 0.0; c.g = 1.0; c.b = 1.0;

  }



  
  for (int ci = 0; ci < center_ls.size()-1;) {
    pc = center_ls[ci];
    if (center_ls.size() > 1){
      pc2 = center_ls[ci+1];
    }
    else {
      return;
    }
    
  
    double heading = atan2(pc2.y()-pc.y(), pc2.x() -pc.x());
    
          lanelet::BasicPoint3d pt_tf[3];
	  
    Eigen::Vector3d axis(0,0,1);
    Eigen::Transform<double, 3, Eigen::Affine> t(Eigen::AngleAxis<double>(heading,axis));
    
    for (int i = 0; i < 3; i++) pt_tf[i] = t*pt[i] + pc;
    

  geometry_msgs::Point gp[3];

  
  for (int i = 0; i < 3; i++) {
      std_msgs::ColorRGBA cn = c;

    gp[i].x = pt_tf[i].x();
    gp[i].y = pt_tf[i].y();
    gp[i].z = pt_tf[i].z();
    marker.points.push_back(gp[i]);
    marker.colors.push_back(cn);
    
  }
  ci = ci+1;
  }
}

void Visualization::publishLaneletDirectionAsMarkerArray(lanelet::Lanelets lanelets, ros::Publisher & pub)
{

  visualization_msgs::MarkerArray marker_array;
  marker_array = Visualization::laneletDirectionAsMarkerArray(lanelets);
  pub.publish(marker_array);
  
}

visualization_msgs::MarkerArray Visualization::laneletDirectionAsMarkerArray(lanelet::Lanelets lanelets)
{

  visualization_msgs::MarkerArray marker_array;
  int ll_dir_count = 0;
  for (auto lli = lanelets.begin(); lli != lanelets.end(); lli++) {

    lanelet::Lanelet ll = *lli;
    
    //      bool road_found = false;
    if (ll.hasAttribute(std::string("turn_direction"))){
      lanelet::Attribute attr = ll.attribute("turn_direction");
      visualization_msgs::Marker marker;
      
      laneletDirectionAsMarker(ll, marker,ll_dir_count, "lanelet direction");

      marker_array.markers.push_back(marker);
      ll_dir_count++;
    }
    
    
  }
  return(marker_array);
  
}

void Visualization::publishAutowareTrafficLightsAsMarkerArray(std::vector<lanelet::autoware::AutowareTrafficLight::Ptr> tl_reg_elems,
							      ros::Publisher & pub)
{
  visualization_msgs::MarkerArray tl_marker_array;
  tl_marker_array = Visualization::autowareTrafficLightsAsMarkerArray(tl_reg_elems);
  pub.publish(tl_marker_array);
}

visualization_msgs::MarkerArray Visualization::autowareTrafficLightsAsMarkerArray(std::vector<lanelet::autoware::AutowareTrafficLight::Ptr> tl_reg_elems)
{

  visualization_msgs::MarkerArray tl_marker_array;
  
  int tl_count = 0;
  for (auto tli = tl_reg_elems.begin(); tli != tl_reg_elems.end(); tli++) {
	    
    lanelet::ConstLineStrings3d lights;
    lanelet::autoware::AutowareTrafficLight::Ptr tl = *tli;
 
    lights = tl->lightBulbs();
     for (auto ls: lights){
      lanelet::ConstLineString3d l = static_cast<lanelet::LineString3d>(ls);

      for (auto pt: l) {

	if (pt.hasAttribute("color")){

	  visualization_msgs::Marker marker;
	  lightAsMarker(pt, marker, tl_count, "traffic_light");
	  tl_marker_array.markers.push_back(marker);

	  tl_count++;
	}
      }
    }
  
  } 


  return(tl_marker_array);
}
void Visualization::publishTrafficLightsAsMarkerArray(std::vector<lanelet::TrafficLight::Ptr> tl_reg_elems, ros::Publisher & pub)
{

  
  visualization_msgs::MarkerArray tl_marker_array;
	
  int tl_count = 0;
  for (auto tli = tl_reg_elems.begin(); tli != tl_reg_elems.end(); tli++) {
	    
    lanelet::LineString3d ls;
  

  int tl_count = 0;
    lanelet::LineStringOrPolygon3d lights = (*tli)->trafficLights().front();

    if (lights.isLineString()) { // traffic ligths can either polygons or linestrings
      ls = static_cast<lanelet::LineString3d>(lights);
      
      int point_count = 0;
      for (auto pi = ls.begin(); pi != ls.end(); pi++) {
	lanelet::Point3d p = *pi;
	
	// visualise traffic lights
	visualization_msgs::Marker marker;
	uint32_t box_shape = visualization_msgs::Marker::CUBE; 
	
	marker.header.frame_id = "map";
	marker.header.stamp = ros::Time();
	marker.ns = "traffic_light";
	marker.id = tl_count;
	marker.type = box_shape;
	marker.pose.position.x = p.x();
	marker.pose.position.y = p.y();
	marker.pose.position.z = p.z();
  
	float s = 0.3;
	
	marker.scale.x = s;
	marker.scale.y = s;
	marker.scale.z = s;
	
	marker.color.r = 0.0f;
	marker.color.g = 0.0f;
	marker.color.b = 0.0f;
	marker.color.a = 1.0f;
	
	if (point_count == 0) marker.color.r = 1.0f;
	else if (point_count == 1) marker.color.g = 1.0f;
	if (point_count == 2) marker.color.b = 1.0f;
	
	marker.lifetime = ros::Duration();
	
	tl_marker_array.markers.push_back(marker);
	point_count++;
	tl_count++;
	
      }
      
      
    } 
  }
  
  pub.publish(tl_marker_array);
}

void Visualization::publishTrafficLightsLineStringsAsMarkerArray(std::vector<lanelet::TrafficLight::Ptr> tl_reg_elems, ros::Publisher & pub)
{

  
  //convert to to an array of linestrings and publish as marker array using exisitng function
  
  int tl_count = 0;
  std::vector<lanelet::ConstLineString3d> line_strings;

  for (auto tli = tl_reg_elems.begin(); tli != tl_reg_elems.end(); tli++) {



    //lanelet::ConstLineStrings3d lights;
    lanelet::TrafficLight::Ptr tl = *tli;
    lanelet::LineString3d ls;
     
    //ls = static_cast<lanelet::LineString3d>(lights);
      
    auto lights = tl->trafficLights();
    for (auto lsp: lights){

   
      if (lsp.isLineString()) { // traffic ligths can either polygons or linestrings
	
	lanelet::ConstLineString3d ls = static_cast<lanelet::LineString3d>(lsp);
	double h = 0.0;
	if (ls.hasAttribute("height")){
	  lanelet::Attribute attr = ls.attribute("height");
	  h = std::stod(attr.value());
	}

	
	line_strings.push_back(ls);
      }
      
      
    }
  }
  
  float line_param[4] = {1.0, 1.0, 1.0, 0.1};
  std_msgs::ColorRGBA c;
  c.r = 1.0; c.g = 1.0; c.b = 1.0; c.a = 1.0;
  Visualization::publishLineStringsAsMarkerArray(line_strings, pub, "tl_line_strings", c, 0.1);
  
}


void Visualization::publishLaneletsAsMarkers(lanelet::Lanelets & lanelets, ros::Publisher & pub,
					     float *lline_param, float *rline_param)
{
  if (lline_param == NULL) { // set defaul values rgb and line width
    lline_param = (float *) malloc (4*sizeof(float));
    
    lline_param[0] = 1.0; lline_param[1] = 0.0; lline_param[2] = 0.0; lline_param[3] = 0.4;
    
  }
   if (rline_param == NULL) {
     
     rline_param = (float *) malloc (4*sizeof(float));
     rline_param[0] = 0.0; rline_param[1] = 0.0; rline_param[2] = 1.0; rline_param[3] = 0.4;
     
   }
   int lcount = 0;

   for (auto li = lanelets.begin(); li !=lanelets.end(); li++) {
     lanelet::ConstLanelet lll = *li;
     
     // if (lcount < max_size)
     Visualization::publishLaneletAsMarkers(lll, pub, lline_param, rline_param, "lanelets", lcount);
     lcount++;
   }
   
   
}



void Visualization::publishLineStringsAsMarkerArray(std::vector<lanelet::ConstLineString3d> line_strings, ros::Publisher & pub,
						    std::string name_space, std_msgs::ColorRGBA c, double lss)
{


  visualization_msgs::MarkerArray ls_marker_array;
  ls_marker_array = Visualization::lineStringsAsMarkerArray(line_strings, name_space, c, lss);
  pub.publish(ls_marker_array);
}

visualization_msgs::MarkerArray  Visualization::lineStringsAsMarkerArray(std::vector<lanelet::ConstLineString3d> line_strings,
									 std::string name_space, std_msgs::ColorRGBA c, double lss)
{


  visualization_msgs::MarkerArray ls_marker_array;
  int ls_count = 0;
  for (auto i = line_strings.begin(); i != line_strings.end(); i++) {
    
    lanelet::ConstLineString3d ls = *i;
    visualization_msgs::Marker ls_marker;

    Visualization::lineString2Marker(ls_count, ls, ls_marker, "map", name_space, c, 0.2);

    ls_marker_array.markers.push_back(ls_marker);
    ls_count++;
    
  }

  return(ls_marker_array);
}
 
void Visualization::publishLaneletsBoundaryAsMarkerArray(lanelet::Lanelets & lanelets, ros::Publisher & pub, std_msgs::ColorRGBA c, bool viz_centerline)
{
  visualization_msgs::MarkerArray lanelets_marker_array = Visualization::laneletsBoundaryAsMarkerArray(lanelets, c, viz_centerline);
   pub.publish(lanelets_marker_array);
}

visualization_msgs::MarkerArray Visualization::laneletsBoundaryAsMarkerArray(lanelet::Lanelets & lanelets,
									     std_msgs::ColorRGBA c, bool viz_centerline)
{

  int lcount = 0;

  double lss = 0.2; // line string size
  visualization_msgs::MarkerArray marker_array;
  for (auto li = lanelets.begin(); li !=lanelets.end(); li++) {
    lanelet::ConstLanelet lll = *li;
     
    lanelet::ConstLineString3d left_ls = lll.leftBound();
    lanelet::ConstLineString3d right_ls = lll.rightBound();
    lanelet::ConstLineString3d center_ls = lll.centerline();
     
     
    visualization_msgs::Marker left_line_strip, right_line_strip, center_line_strip;
    
    Visualization::lineString2Marker(lcount, left_ls, left_line_strip, "map", "left_lane_bound", c, lss);
    Visualization::lineString2Marker(lcount,right_ls, right_line_strip, "map", "right_lane_bound", c, lss);
    marker_array.markers.push_back(left_line_strip);
    marker_array.markers.push_back(right_line_strip);


    if (viz_centerline) {
      Visualization::lineString2Marker(lcount, center_ls, center_line_strip, "map", "center_lane_line", c, lss*0.5);
      marker_array.markers.push_back(center_line_strip);
    }
    lcount++;
     
  }
  return marker_array;
}




void Visualization::publishTrafficLightsAsPolygonArray(std::vector<lanelet::TrafficLight::Ptr> tl_reg_elems, ros::Publisher & pub, int seq, double scale)
{

  
  //convert to to an array of linestrings and publish as marker array using exisitng function
  
  int tl_count = 0;
  std::vector<lanelet::ConstLineString3d> line_strings;
  jsk_recognition_msgs::PolygonArray traffic_lights_polygon_array;


  traffic_lights_polygon_array.header.frame_id = "map";
  traffic_lights_polygon_array.header.seq = seq;
  traffic_lights_polygon_array.header.stamp = ros::Time();

  for (auto tli = tl_reg_elems.begin(); tli != tl_reg_elems.end(); tli++) {



    //lanelet::ConstLineStrings3d lights;
    lanelet::TrafficLight::Ptr tl = *tli;
    lanelet::LineString3d ls;
     
    //ls = static_cast<lanelet::LineString3d>(lights);
      
    auto lights = tl->trafficLights();
    for (auto lsp: lights){

      if (lsp.isLineString()) { // traffic ligths can either polygons or linestrings
	
	lanelet::ConstLineString3d ls = static_cast<lanelet::LineString3d>(lsp);

	geometry_msgs::PolygonStamped polygon;
	//     std::cerr << "lanelet to poluygon\n";
	
	Visualization::trafficLight2PolygonStamped(tl_count, ls, polygon, "map", scale);
	// std::cerr << "before add to arrays\n";
	traffic_lights_polygon_array.polygons.push_back(polygon);
	traffic_lights_polygon_array.labels.push_back(tl_count);
	traffic_lights_polygon_array.likelihood.push_back(0.4);
	tl_count++;
      }

	
    }
      
      
  }
  
  
  //float line_param[4] = {1.0, 1.0, 1.0, 0.1};
  
  //Visualization::publishLineStringsAsMarkerArray(line_strings, pub, "tl_line_strings", line_param);
  pub.publish(traffic_lights_polygon_array);
}

void Visualization::publishTrafficLightsAsTriangleMarkerArray(std::vector<lanelet::TrafficLight::Ptr> tl_reg_elems, ros::Publisher & pub,   std_msgs::ColorRGBA c, ros::Duration duration, double scale)
{

  visualization_msgs::MarkerArray marker_array;
  marker_array = Visualization::trafficLightsAsTriangleMarkerArray(tl_reg_elems, c, duration, scale);
  
  pub.publish(marker_array);
}

visualization_msgs::MarkerArray Visualization::trafficLightsAsTriangleMarkerArray(std::vector<lanelet::TrafficLight::Ptr> tl_reg_elems,   std_msgs::ColorRGBA c, ros::Duration duration, double scale)
{

  
  //convert to to an array of linestrings and publish as marker array using exisitng function
  
  int tl_count = 0;
  std::vector<lanelet::ConstLineString3d> line_strings;
  visualization_msgs::MarkerArray marker_array;

  for (auto tli = tl_reg_elems.begin(); tli != tl_reg_elems.end(); tli++) {



    //lanelet::ConstLineStrings3d lights;
    lanelet::TrafficLight::Ptr tl = *tli;
    lanelet::LineString3d ls;
     
    //ls = static_cast<lanelet::LineString3d>(lights);
      
    auto lights = tl->trafficLights();
    for (auto lsp: lights){

      if (lsp.isLineString()) { // traffic ligths can either polygons or linestrings
	
	lanelet::ConstLineString3d ls = static_cast<lanelet::LineString3d>(lsp);

	visualization_msgs::Marker marker;
	
	Visualization::trafficLight2TriangleMarker(tl_count, ls, marker, "traffic_light_triangle", c,duration,scale);
	marker_array.markers.push_back(marker);
	tl_count++;
      }

	
    }
      
    
  }
  
  
  return(marker_array);
}



void Visualization::publishLaneletsAsPolygonArray(lanelet::Lanelets & lanelets, ros::Publisher & pub)
{
   int lcount = 0;
   jsk_recognition_msgs::PolygonArray lanelets_polygon_array;


   lanelets_polygon_array.header.frame_id = "map";
   lanelets_polygon_array.header.seq = 200001;
   lanelets_polygon_array.header.stamp = ros::Time();

   //uint32 i = 0:
   for (auto li = lanelets.begin(); li !=lanelets.end(); li++) {

     lanelet::Lanelet ll = *li;

     geometry_msgs::PolygonStamped polygon;
     Visualization::lanelet2PolygonStamped(lcount, ll, polygon, "map");
     lanelets_polygon_array.polygons.push_back(polygon);
     lanelets_polygon_array.labels.push_back(lcount);
     lanelets_polygon_array.likelihood.push_back(0.4);
     lcount++;
    
   }
   pub.publish(lanelets_polygon_array);
}



void Visualization::publishLaneletAsMarkers(lanelet::ConstLanelet & lanelet, ros::Publisher &pub,
					   float *lline_param, float *rline_param, std::string ns, int id)
{
  
  
  visualization_msgs::Marker left_line_strip, left_points, right_line_strip, right_points;
  
  lanelet::ConstLineString3d left_ls = lanelet.leftBound();
  lanelet::ConstLineString3d right_ls = lanelet.rightBound();

  std_msgs::ColorRGBA c;
  c.r =  lline_param[0]; c.g = lline_param[1]; c.b = lline_param[2]; c.a = lline_param[3];

  
  Visualization::lineString2Marker(id*1000, left_ls, left_line_strip, "map", "left_lane_bound", c, 0.2);
  Visualization::lineString2Marker(id*1000+1,right_ls, right_line_strip, "map", "right_lane_bound", c, 0.2);
  
  pub.publish(left_line_strip);
  pub.publish(right_line_strip);


}


void Visualization::publishLaneletsAsTriangleMarkerArray(std::string ns, lanelet::Lanelets & lanelets, ros::Publisher & pub, std_msgs::ColorRGBA c)
{

   
  visualization_msgs::MarkerArray marker_array;

  marker_array = Visualization::laneletsAsTriangleMarkerArray(ns, lanelets, c);
  pub.publish(marker_array);
}

visualization_msgs::MarkerArray Visualization::laneletsAsTriangleMarkerArray(std::string ns, lanelet::Lanelets & lanelets,
									     std_msgs::ColorRGBA c)
{

  
  visualization_msgs::MarkerArray marker_array;
  visualization_msgs::Marker marker;




  marker.header.frame_id = "map";
  marker.header.stamp = ros::Time();
  marker.ns = ns;
  marker.id = 0;
  marker.type = visualization_msgs::Marker::TRIANGLE_LIST;

  marker.lifetime = ros::Duration();
 
  marker.pose.position.x = 0.0;//p.x();
  marker.pose.position.y = 0.0;//p.y();
  marker.pose.position.z = 0.0;//p.z();
  marker.scale.x = 1.0;
  marker.scale.y = 1.0;
  marker.scale.z = 1.0;
  marker.color.r = 1.0f;
  marker.color.g = 1.0f;
  marker.color.b = 1.0f;
  marker.color.a = 1.0f;

  
  for (auto ll : lanelets) {

    
    std::vector<geometry_msgs::Polygon> triangles;
    lanelet_utils::Query::lanelet2Triangle(ll, triangles);

    for (auto tri : triangles){
      
      
      //  cl.r = 0.7; cl.g = 0.7; cl.b = 0.7; cl.a = 0.5;
      geometry_msgs::Point tri0[3];

      for (int i = 0; i < 3; i++) {
	setGeomMsgPtFromPt32(tri0[i], tri.points[i]);

	marker.points.push_back(tri0[i]);
	marker.colors.push_back(c);
      }

      
    }
  }
  marker_array.markers.push_back(marker);
      
  return(marker_array);
}





void Visualization::trafficLight2PolygonStamped(int id, lanelet::ConstLineString3d ls, geometry_msgs::PolygonStamped & polygon,
						std::string frame_id, double scale)
{
  polygon.header.frame_id = frame_id;
  polygon.header.seq = id;
  polygon.header.stamp = ros::Time();
  
  double h = 0.7;
  if (ls.hasAttribute("height")){
    lanelet::Attribute attr = ls.attribute("height");
    h = std::stod(attr.value());
  }
 
  Eigen::Vector3d v[4];
  v[0] << ls.front().x(), ls.front().y(), ls.front().z();
  v[1] << ls.back().x(), ls.back().y(),ls.back().z() ;
  v[2] << ls.back().x(), ls.back().y(),ls.back().z()+h ;
  v[3] << ls.front().x(), ls.front().y(),ls.front().z()+h;

  Eigen::Vector3d c = (v[0]+v[1] + v[2] + v[3])/4;

  if (scale > 0.0 && scale != 1.0) {
    for (int i = 0; i < 4; i++){
      v[i] = (v[i] - c)*scale + c;
    }
  }
  geometry_msgs::Point32 pts[4];
  for (int i =0; i < 4; i++) {
    setGeomMsgPt32(pts[i], v[i]);
    polygon.polygon.points.push_back(pts[i]);
  }
  
}

void Visualization::trafficLight2TriangleMarker(int id, lanelet::ConstLineString3d ls, visualization_msgs::Marker & marker,
						std::string ns,   std_msgs::ColorRGBA cl, ros::Duration duration, double scale)
{


  marker.header.frame_id = "map";
  marker.header.stamp = ros::Time();
  marker.ns = ns;
  marker.id = id;
  marker.type = visualization_msgs::Marker::TRIANGLE_LIST;

  marker.lifetime =duration;
 
  marker.pose.position.x = 0.0;//p.x();
  marker.pose.position.y = 0.0;//p.y();
  marker.pose.position.z = 0.0;//p.z();
  marker.scale.x = 1.0;
  marker.scale.y = 1.0;
  marker.scale.z = 1.0;
  marker.color.r = 1.0f;
  marker.color.g = 1.0f;
  marker.color.b = 1.0f;
  marker.color.a = 1.0f;

  double h = 0.7;
  if (ls.hasAttribute("height")){
    lanelet::Attribute attr = ls.attribute("height");
    h = std::stod(attr.value());
    
  }

  // construct triangles and add to marker

  // define polygon of traffic light border
  Eigen::Vector3d v[4];
  v[0] << ls.front().x(), ls.front().y(), ls.front().z();
  v[1] << ls.back().x(), ls.back().y(),ls.back().z() ;
  v[2] << ls.back().x(), ls.back().y(),ls.back().z()+h ;
  v[3] << ls.front().x(), ls.front().y(),ls.front().z()+h;

  Eigen::Vector3d c = (v[0]+v[1] + v[2] + v[3])/4;

  if (scale > 0.0 && scale != 1.0) {
    for (int i = 0; i < 4; i++){
      v[i] = (v[i] - c)*scale + c;
    }
  }
  geometry_msgs::Point tri0[3];
  setGeomMsgPt(tri0[0], v[0]);
  setGeomMsgPt(tri0[1], v[1]);
  setGeomMsgPt(tri0[2], v[2]);
  geometry_msgs::Point tri1[3];
  setGeomMsgPt(tri1[0], v[0]);
  setGeomMsgPt(tri1[1], v[2]);
  setGeomMsgPt(tri1[2], v[3]);

  //  cl.r = 0.7; cl.g = 0.7; cl.b = 0.7; cl.a = 0.5;

  for (int i = 0; i < 3; i++) {
    marker.points.push_back(tri0[i]);
    marker.colors.push_back(cl);
  }
  for (int i = 0; i < 3; i++) {
    marker.points.push_back(tri1[i]);
    marker.colors.push_back(cl);
  }
}

void Visualization::lanelet2PolygonStamped(int id, lanelet::Lanelet ll, geometry_msgs::PolygonStamped & polygon,
					   std::string frame_id)
{
  polygon.header.frame_id = frame_id;
  polygon.header.seq = id;
  polygon.header.stamp = ros::Time();

  
  lanelet::ConstLineString3d left_ls = ll.leftBound();
  lanelet::ConstLineString3d right_ls = ll.rightBound();

  int polygon_size= left_ls.size() +right_ls.size();
  // polygon.polygon.points.resize(polygon_size);
  //  geometry_msgs::Point32 p;
  
  for (auto i = left_ls.begin(); i != left_ls.end(); i++) {
 geometry_msgs::Point32 p;
 
    p.x = (*i).x();
    p.y = (*i).y();
    p.z =(*i).z(); // peoria data has no z
    polygon.polygon.points.push_back(p);
    
  }
  
  for (auto i = right_ls.end(); i != right_ls.begin();) {
    i--;
     geometry_msgs::Point32 p;
 
   p.x = (*i).x();
    p.y = (*i).y();
    p.z =(*i).z(); // peoria data has no z
    polygon.polygon.points.push_back(p);
    
  }
  
  
}

void Visualization::lineString2Marker(int lane_id, lanelet::ConstLineString3d ls,
				      visualization_msgs::Marker& line_strip,
				      std::string frame_id, std::string ns, std_msgs::ColorRGBA c,  float lss)
{
  
  
  line_strip.header.frame_id = frame_id;
  line_strip.header.stamp = ros::Time();
  line_strip.ns = ns;
  line_strip.action = visualization_msgs::Marker::ADD;
  
  line_strip.pose.orientation.w = 1.0;
  line_strip.id = lane_id;

  line_strip.type = visualization_msgs::Marker::LINE_STRIP;

  line_strip.scale.x = lss; 

  line_strip.color = c;
  /*
  line_strip.color.r = lr;
  line_strip.color.g = lg;
  line_strip.color.b = lb;
  line_strip.color.a = 1.0f;
  */
  
  // fill out lane line
  for (auto i = ls.begin(); i != ls.end(); i++){
    geometry_msgs::Point p;
    p.x = (*i).x();
    p.y = (*i).y();
    p.z = (*i).z();
    //if (fake_z) p.z = 118.0;
    //  points.points.push_back(p);
    line_strip.points.push_back(p);
  }
}

