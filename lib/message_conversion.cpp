#include <ros/ros.h>
#include <lanelet2_core/primitives/Lanelet.h>
#include <lanelet2_io/Exceptions.h>
#include <lanelet2_io/Projection.h>
#include <lanelet2_io/io_handlers/OsmHandler.h>
#include <lanelet2_io/io_handlers/OsmFile.h>
#include <lanelet2_io/io_handlers/Serialize.h>
#include <lanelet2_projection/UTM.h>
#include <lanelet2_extension/projection/mgrs_projector.hpp>
#include <lanelet2_extension/io/message_conversion.hpp>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include <sstream>
#include <string>

using namespace lanelet_utils;


//-------------------------------------------------------------------------
//
// toXMLMsg()
// takes a projector as a parameter as assume most cases have loaded directly
// from file - in which case have created a projector already
//
//-------------------------------------------------------------------------

int Map::toXMLMsg(lanelet::LaneletMapPtr map, lanelet2_msgs::MapXML& msg, lanelet::Projector& projector)
{
  // convert laneletmap into osm file data structure
  std::unique_ptr<lanelet::osm::File> map_osm;
  lanelet::ErrorMessages errors;

  // base class of IOHandler has projector as an argument for the constructor - must have
  // IOHandler -> Writer -> OsmWriter
  lanelet::io_handlers::OsmWriter osm_writer(projector);

  map_osm = osm_writer.toOsmFile(*map, errors);  // write map to an osm file data structure

  std::stringstream ss;
  std::unique_ptr<pugi::xml_document> pugi_xml_doc;

  pugi_xml_doc = lanelet::osm::write(*map_osm); // write osm file data to a pugi_xml doc
  pugi_xml_doc->save(ss); // write xml to stringstream

  //std::string all_xml = ss.str();  // then to string for ros message
  msg.xml_map = ss.str();
  std::cout << ss.str().size() << std::endl;
  int status = 1;
  return status;
}

void Map::toBinMsg(const lanelet::LaneletMapPtr map, lanelet2_msgs::MapBin& msg)
{
  std::stringstream ss;
  boost::archive::binary_oarchive oa(ss);
  oa << *map;
  auto id_counter = lanelet::utils::getId();
  oa << id_counter;
  msg.data = ss.str();
  std::cout << ss.str().size() << std::endl;
}

//-------------------------------------------------------------------------
//
//
//
//-------------------------------------------------------------------------

int Map::fromXMLMsg(lanelet2_msgs::MapXML msg, lanelet::LaneletMapPtr& map)
{
  ROS_INFO_STREAM( "Recieved lanelet map xml string message size = " << msg.xml_map.size());

    int status = 0;

  lanelet::ErrorMessages errors;

  std::shared_ptr<lanelet::Projector> projector_ptr;

  // got map in xml form
  // now reverse the steps used to publish
  // string -> srtingstream -> xml doc -> osm format ->lanelet map

  if(msg.projector_type == lanelet2_msgs::MapXML::MGRS)
  {
    projector_ptr = std::make_shared<lanelet::projection::MGRSProjector>();
  }
  else if(msg.projector_type == lanelet2_msgs::MapXML::UTM)
  {
    projector_ptr = std::make_shared<lanelet::projection::UtmProjector>(lanelet::Origin({msg.origin_lat, msg.origin_lon}));
  }
  else
  {
    ROS_ERROR_STREAM( "Projector " << msg.projector_type << "is not supported." << std::endl
                   << "Using MGRS projector instead." << std::endl);
    projector_ptr = std::make_shared<lanelet::projection::MGRSProjector>();
  }

  std::stringstream ss;
  ss << msg.xml_map;
  pugi::xml_document pugi_xml_doc;
  pugi::xml_parse_result result = pugi_xml_doc.load(ss);
  if (result.status == pugi::status_ok) {
    // failed beacuse no default projector set, now works
    lanelet::osm::File map_osm = lanelet::osm::read(pugi_xml_doc);  // read a map in to an osm file data structure
    lanelet::io_handlers::OsmParser osm_parser(*projector_ptr);

    map = osm_parser.fromOsmFile(map_osm, errors);
    status = 1;
  }
  else {
    std::cerr << "WARNING xmlMapToLaneletMap(): could not load xml string stream into xml document\n";
  }
  return status;

}


void Map::fromBinMsg(const lanelet2_msgs::MapBin msg, lanelet::LaneletMapPtr& map)
{
  std::stringstream ss;
  std::unique_ptr<lanelet::LaneletMap> laneletMap = std::make_unique<lanelet::LaneletMap>();
  // map = std::make_shared<lanelet::LaneletMap>();
  ss << msg.data;
  boost::archive::binary_iarchive oa(ss);
  oa >> *laneletMap;
  lanelet::Id id_counter;
  oa >> id_counter;
  lanelet::utils::registerId(id_counter);
  map = std::move(laneletMap); 
  // for(auto lanelet: map->laneletLayer)
  //   std::cout << lanelet << std::endl;
  
}
