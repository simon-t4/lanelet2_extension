#pragma once
#include <lanelet2_io/Exceptions.h>
#include <lanelet2_io/Projection.h>
#include <lanelet2_core/primitives/Lanelet.h>
#include <lanelet2_io/Io.h>
#include <lanelet2_io/io_handlers/Factory.h>
#include <lanelet2_io/io_handlers/Writer.h>
#include <lanelet2_io/io_handlers/Serialize.h>

#include <lanelet2_io/io_handlers/OsmHandler.h>
#include <lanelet2_io/io_handlers/OsmFile.h>
#include <lanelet2_projection/UTM.h>
#include <lanelet2_core/LaneletMap.h>
#include <lanelet2_core/primitives/BasicRegulatoryElements.h>
#include <lanelet2_core/primitives/LineString.h>
#include <lanelet2_core/primitives/LineStringOrPolygon.h>
#include <lanelet2_core/primitives/Lanelet.h>
//#include <Eigen/Eigen>

#include <jsk_recognition_msgs/PolygonArray.h>
#include <geometry_msgs/PolygonStamped.h>

#include <cstdio>
#include <sstream>

namespace lanelet_utils {




  class Visualization {
  public:

    static void lineString2Marker(int lane_id, lanelet::ConstLineString3d ls,
				  visualization_msgs::Marker& line_strip,
				  std::string frame_id, std::string ns, std_msgs::ColorRGBA c, float lss= 0.1);


    static void publishTrafficLightsAsTriangleMarkerArray(std::vector<lanelet::TrafficLight::Ptr> tl_reg_elems, ros::Publisher & pub,   std_msgs::ColorRGBA cl, ros::Duration duration = ros::Duration(), double scale = 1.0);

    static void publishLaneletDirectionAsMarkerArray(lanelet::Lanelets lanelets, ros::Publisher & pub);

    static void publishLineStringsAsMarkerArray(std::vector<lanelet::ConstLineString3d> line_strings, ros::Publisher &pub, std::string name_space,
						std_msgs::ColorRGBA c, double lss);
    static  void publishTrafficLightsLineStringsAsMarkerArray(std::vector<lanelet::TrafficLight::Ptr> tl_reg_elems, ros::Publisher & pub);

    static void publishTrafficLightsAsMarkerArray(std::vector<lanelet::TrafficLight::Ptr> tl_reg_elems, ros::Publisher & pub);

    static void publishAutowareTrafficLightsAsMarkerArray(std::vector<lanelet::autoware::AutowareTrafficLight::Ptr> tl_reg_elems,
							  ros::Publisher & pub);

    static void trafficLight2PolygonStamped(int id, lanelet::ConstLineString3d ls, geometry_msgs::PolygonStamped & polygon,
    					    std::string frame_id, double scale = 1.0);
    static void trafficLight2TriangleMarker(int id, lanelet::ConstLineString3d ls, visualization_msgs::Marker & marker,
					    std::string ns,   std_msgs::ColorRGBA cl, ros::Duration duration = ros::Duration(), double scale = 1.0);
    
    static void publishTrafficLightsAsPolygonArray(std::vector<lanelet::TrafficLight::Ptr> tl_reg_elems, ros::Publisher & pub, int seq = 30001, double scale = 1.0);

    static void publishLaneletAsMarkers(lanelet::ConstLanelet & lanelet, ros::Publisher &pub,
					float *lline_color, float *rline_color, std::string ns, int id);

       static void publishLaneletsAsPolygonArray(lanelet::Lanelets & lanelets, ros::Publisher & pub);
    static void publishLaneletsAsTriangleMarkerArray(std::string ns, lanelet::Lanelets & lanelets, ros::Publisher & pub, std_msgs::ColorRGBA cl);

    static void publishLaneletsAsMarkers(lanelet::Lanelets & lanelets, ros::Publisher &pub,
					 float *lline_param = NULL, float *rline_param = NULL);
    static void publishLaneletsBoundaryAsMarkerArray(lanelet::Lanelets & lanelets, ros::Publisher &pub,
						     std_msgs::ColorRGBA c, bool viz_centerline);



    static visualization_msgs::MarkerArray laneletsBoundaryAsMarkerArray(lanelet::Lanelets & lanelets,
									 std_msgs::ColorRGBA c,
									 bool viz_centerline);

    static visualization_msgs::MarkerArray laneletsAsTriangleMarkerArray(std::string ns, lanelet::Lanelets & lanelets,
									 std_msgs::ColorRGBA c);


    static visualization_msgs::MarkerArray laneletDirectionAsMarkerArray(lanelet::Lanelets lanelets);
    
    static visualization_msgs::MarkerArray  lineStringsAsMarkerArray(std::vector<lanelet::ConstLineString3d> line_strings,
								     std::string name_space,
								     std_msgs::ColorRGBA c, double lss);

    
    static visualization_msgs::MarkerArray autowareTrafficLightsAsMarkerArray(std::vector<lanelet::autoware::AutowareTrafficLight::Ptr> tl_reg_elems);

    static visualization_msgs::MarkerArray  trafficLightsAsTriangleMarkerArray(std::vector<lanelet::TrafficLight::Ptr> tl_reg_elems,   std_msgs::ColorRGBA c, ros::Duration duration = ros::Duration(), double scale = 1.0);

    static void lanelet2PolygonStamped(int id, lanelet::Lanelet ll, geometry_msgs::PolygonStamped & polygon,
				       std::string frame_id);

    
  private:
    Visualization(); // constructor provate - do not instantiate class
  };

}

