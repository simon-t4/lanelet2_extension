#pragma once

#include <lanelet2_core/primitives/BasicRegulatoryElements.h>
#include <lanelet2_core/primitives/Lanelet.h>
#include <vector>
namespace lanelet{

namespace autoware {

struct AutowareRoleNameString{
  static constexpr const char LightBulbs[] = "light_bulbs";
  // using RoleNamesItem = std::pair<const char*, const RoleName>;
  // static constexpr RoleNamesItem Map[]{{LightBulbs, AutowareRoleName::LightBulbs}};
};

class AutowareTrafficLight : public lanelet::TrafficLight {
public:
    using Ptr = std::shared_ptr<AutowareTrafficLight>;
    static constexpr char RuleName[] = "traffic_light";

    //! Directly construct a stop line from its required rule parameters.
    //! Might modify the input data in oder to get correct tags.
    static Ptr make(Id id, const AttributeMap& attributes, const LineStringsOrPolygons3d& trafficLights,
                    const Optional<LineString3d>& stopLine = {}, const std::vector<Optional<LineString3d>>& lightBulbs = {{}}) {
      return Ptr{new AutowareTrafficLight(id, attributes, trafficLights, stopLine, lightBulbs)};
    }

    /**
     * @brief get the relevant traffic light bulbs
     * @return the traffic light bulbs
     *
     * There might be multiple traffic light bulbs but they are required to show the
     * same signal.
     */
    // ConstLineStringsOrPolygons3d lightBulbs() const;
    ConstLineStrings3d lightBulbs() const;

    /**
     * @brief add a new traffic light bulb
     * @param primitive the traffic light bulb to add
     *
     * Traffic light bulbs are represented as linestrings with each point expressing position of each light bulb (lamp).
     */
    void addLightBulbs(const LineStringOrPolygon3d& primitive);

    /**
     * @brief remove a traffic light bulb
     * @param primitive the primitive
     * @return true if the traffic light bulb existed and was removed
     */
    bool removeLightBulbs(const LineStringOrPolygon3d& primitive);

private:

    // the following lines are required so that lanelet2 can create this object when loading a map with this regulatory
    // element
    friend class lanelet::RegisterRegulatoryElement<AutowareTrafficLight>;
    AutowareTrafficLight(Id id, const AttributeMap& attributes, const LineStringsOrPolygons3d& trafficLights,
                 const Optional<LineString3d>& stopLine, const std::vector<Optional<LineString3d>>& lightBulbs);
     explicit AutowareTrafficLight(const lanelet::RegulatoryElementDataPtr& data);
};
static lanelet::RegisterRegulatoryElement<AutowareTrafficLight> regAutowareTraffic;


  // moved to lanelet2_extension/lib/autoware_traffic_light.cpp to avoid multiple defintion errors
  /* 
#if __cplusplus < 201703L
constexpr char AutowareTrafficLight::RuleName[];      // instanciate string in cpp file
#endif
  */
}
}
