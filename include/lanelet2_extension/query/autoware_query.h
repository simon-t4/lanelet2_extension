#pragma once
#include <lanelet2_io/Exceptions.h>
#include <lanelet2_io/Projection.h>
#include <lanelet2_core/primitives/Lanelet.h>
#include <lanelet2_io/Io.h>
#include <lanelet2_io/io_handlers/Factory.h>
#include <lanelet2_io/io_handlers/Writer.h>
#include <lanelet2_io/io_handlers/Serialize.h>

#include <lanelet2_io/io_handlers/OsmHandler.h>
#include <lanelet2_io/io_handlers/OsmFile.h>
#include <lanelet2_projection/UTM.h>
#include <lanelet2_core/LaneletMap.h>
#include <lanelet2_core/primitives/BasicRegulatoryElements.h>
#include <lanelet2_core/primitives/LineString.h>
#include <lanelet2_core/primitives/LineStringOrPolygon.h>
#include <lanelet2_core/primitives/Lanelet.h>
//#include <Eigen/Eigen>

#include <jsk_recognition_msgs/PolygonArray.h>
#include <geometry_msgs/PolygonStamped.h>

#include <lanelet2_extension/regulatory_elements/autoware_traffic_light.hpp>

#include <cstdio>
#include <sstream>

namespace lanelet_utils {


  class Query {
  public:


    static void lanelet2Triangle(lanelet::Lanelet ll, std::vector<geometry_msgs::Polygon> & triangles);

    static void lanelet2Polygon(lanelet::Lanelet ll, geometry_msgs::Polygon& polygon);

    static lanelet::Lanelets laneletLayer(lanelet::LaneletMapPtr ll_Map);
    static lanelet::Lanelets subtypeLanelets(lanelet::Lanelets lls, const char subtype[]);
    static lanelet::Lanelets crosswalkLanelets(lanelet::Lanelets lls);
    static lanelet::Lanelets roadLanelets(lanelet::Lanelets lls);

    static std::vector<lanelet::TrafficLight::Ptr> trafficLights(lanelet::Lanelets lanelets);
    static std::vector<lanelet::autoware::AutowareTrafficLight::Ptr> autowareTrafficLights(lanelet::Lanelets lanelets);

    static std::vector<lanelet::ConstLineString3d> stopLinesLanelets(lanelet::Lanelets lanelets);
    static std::vector<lanelet::ConstLineString3d> stopLinesLanelet(lanelet::Lanelet ll);
  private:
    Query(); // constructor private - do not instantiate class
  };
}

