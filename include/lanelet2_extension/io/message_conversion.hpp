#pragma once
#include <lanelet2_core/LaneletMap.h>
#include <lanelet2_msgs/MapXML.h>
#include <lanelet2_msgs/MapBin.h> 

namespace lanelet_utils {

  class Map {
  public:

    static int toXMLMsg(lanelet::LaneletMapPtr map, lanelet2_msgs::MapXML& msg, lanelet::Projector& projector);
    static int fromXMLMsg(lanelet2_msgs::MapXML msg, lanelet::LaneletMapPtr& map);
    
    static void toBinMsg(const lanelet::LaneletMapPtr map, lanelet2_msgs::MapBin& msg);
    static void fromBinMsg(const lanelet2_msgs::MapBin msg, lanelet::LaneletMapPtr& map);
    
  private:
    Map(); //contructor private - do not instatiate class
  };

}
