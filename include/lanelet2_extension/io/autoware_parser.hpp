#include <lanelet2_io/io_handlers/OsmHandler.h>

namespace lanelet
{
namespace io_handlers
{
  class AutowareOsmParser : public OsmParser {
   public:
    using OsmParser::OsmParser;

    std::unique_ptr<LaneletMap> parse(const std::string& filename, ErrorMessages& errors) const;

    // std::unique_ptr<LaneletMap> fromOsmFile(const osm::File& file, ErrorMessages& errors) const;

    static constexpr const char* extension() { return ".osm"; }

    static constexpr const char* name() { return "autoware_osm_handler"; }
  };

}
}
